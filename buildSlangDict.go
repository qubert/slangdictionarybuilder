package main

import (
	"encoding/json"
	"fmt"
	"github.com/anaskhan96/soup"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var linkRe = regexp.MustCompile(`(?m)((f|ht)tp(s?)://(.*)[\S]+)|(\[.+\]\(.+\))`)
var punct string
var dict map[string]string

//TODO from config....
var slangDict string = "slangDictionary/slangDictionary.json"
var curseDict string = "slangDictionary/curseDictionary.json"

func fetchCurseDict(link string, dict map[string]string) {
	resp, err := soup.Get("https://www.noswearing.com/dictionary/" + link)
	if err != nil {
		os.Exit(1)
	}
	doc := soup.HTMLParse(resp)
	all := doc.FindAll("td")
	for _, tab := range all {
		defs := tab.FindAll("a")
		for _, def := range defs {
			if strings.Trim(def.Attrs()["name"], " ") != "" {
				dict[def.Attrs()["name"]] = "Toxic"
			}
		}
	}
}

func fetchSlangDict(link string, dict map[string]string) {
	resp, err := soup.Get("https://www.noslang.com/dictionary/" + link)
	if err != nil {
		os.Exit(1)
	}
	doc := soup.HTMLParse(resp)
	meanings := doc.FindAll("abbr", "class", "dictonary-abbr")
	for _, meaning := range meanings {
		abbr := strings.Trim(meaning.Find("dt").Text(), ": \n")
		if abbr == "" {
			fmt.Fprintln(os.Stderr, "Empty Abbr(text) For:", meaning.Find("dt").Attrs(), meaning.Attrs()["title"])
		} else {
			dict[abbr] = meaning.Attrs()["title"]
		}
	}
}

func Build() {
	if _, err := os.Stat(slangDict); os.IsNotExist(err) {
		dictionaryDirectory, _ := filepath.Split(slangDict)
		os.Mkdir(dictionaryDirectory, 0755)
		// path/to/whatever does not exist
		links := [...]string{"1", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
		dict = make(map[string]string)
		f, err := os.OpenFile(slangDict, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		enc := json.NewEncoder(f)
		for _, link := range links {
			fetchSlangDict(link, dict)
		}
		enc.Encode(dict)
	}
	if _, err := os.Stat(curseDict); os.IsNotExist(err) {
		// dictionaryDirectory, _ := filepath.Split(curseDict)
		// os.Mkdir(dictionaryDirectory, 0755)
		// path/to/whatever does not exist
		links := [...]string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
		dict = make(map[string]string)
		f, err := os.OpenFile(curseDict, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		enc := json.NewEncoder(f)
		for _, link := range links {
			fetchCurseDict(link, dict)
		}
		enc.Encode(dict)
	}
}

func main() {
	Build()
}
